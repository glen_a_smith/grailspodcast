dataSource {
    pooled = true
    driverClassName = "org.h2.Driver"
    username = "sa"
    password = ""
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
	development {
		dataSource {
			dbCreate = "update"
            pooled = false
            driverClassName = "org.postgresql.Driver"
            url = "jdbc:postgresql://192.168.1.7/grailspodcast.dev"
            username = "glen"
            password = "password"
		}
	}
	test {
		dataSource {
			dbCreate = "update"
			url = "jdbc:hsqldb:mem:testDb"
		}
	}
	production {
		dataSource {
			dbCreate = "update"
            jndiName = "java:comp/env/jdbc/grailspodcast"
		}
	}
}
