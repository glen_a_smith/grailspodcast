class BootStrap {

    def init = { servletContext ->

        if (false) { // import old data rescued from feedburner feed

            println "Sparking up old entries..."

            def sfi = new com.sun.syndication.io.SyndFeedInput()
            FileInputStream fis = new FileInputStream("/data/java/grailspodcast/feed_historical.xml")
            def feedReader = new com.sun.syndication.io.XmlReader(fis)
            def sf = sfi.build(feedReader)

            for (e in sf.entries) {

                String title = e.title
                String description = e.description?.value
                if (!description) {  // mustn't be rss... could be atom
                    description = e.contents[0]?.value
                }
                String link = e.link
                Date publishedDate = e.publishedDate


                println "${publishedDate} --  ${title} -- ${link}"
                Entry entry = new Entry(title: title, content: description, created: publishedDate)
                if (e.enclosures) {
                    def enclosure = e.enclosures[0]
                    entry.enclosureLength = enclosure.length
                    entry.enclosureURL = enclosure.url.toString()
                }
                if (entry.save(flush: true)) {
                    println "Created new entry: ${entry.title}"
                } else {
                    println "Error on entry: ${entry.title} -- ${entry.errors}"
                }

            }

        }

    }
    def destroy = {

    }

} 