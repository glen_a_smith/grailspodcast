import java.text.SimpleDateFormat

class Entry {
	String title
	String content
	Date created
	String tags = "groovy grails"
	String enclosureURL
	Integer enclosureLength
	String enclosureType = "audio/mpeg"
	
	static constraints = {
		title()
		content(widget:'textarea', maxSize:30000)
		created()
		tags(blank:false)
		enclosureURL(nullable:true)
		enclosureLength(nullable:true)
		enclosureType(nullable:true)
	}
	
	def getHumanReadableDate()
	{
		def dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy - HH:mm")
		def nowString = dateFormat.format(created) + ' UTC';
	}
	
	def getFormattedTags()
	{
		tags?.split(' ').collect { it.toUpperCase()}.join(' &middot; ')
	}
}
