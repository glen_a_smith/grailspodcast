<html>
    <head>
        <title><g:layoutTitle default="iPhone" /></title>
        <style type="text/css" media="screen">@import "${createLinkTo(dir:'js/iui',file:'iui.css')}";</style>
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
        <link rel="apple-touch-icon" href="${createLinkTo(dir:'images',file:'apple-touch-icon.png')}"/>
        <link rel="shortcut icon" href="${createLinkTo(dir:'images',file:'favicon.ico')}" type="image/x-icon" />
        <g:layoutHead />
        <g:javascript library="iui" />
        <g:javascript library="application" />				
    </head>
    <body>
        <g:layoutBody />		
    </body>	
</html>