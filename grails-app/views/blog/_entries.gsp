<g:each in="${entries}" var="entry">

  <div class="entry" id="entry${entry.id}">
    <div class="meta">
      <span class="left">${entry.humanReadableDate}</span>
${entry.formattedTags}

    </div>
    <div class="title">
<%= link(action:'id', id:entry.id ) { entry.title.encodeAsHTML() }%>
    </div>
    <div class="content">
${entry.content}
      <g:if test="${entry.enclosureURL}">
        <div class="enclosure"><a href="${entry.enclosureURL}">${entry.enclosureURL}</a></div>
      </g:if>
      <!-- class feedburnerFlareBlock in CSS -->
      <script src="http://feeds.feedburner.com/~s/grailspodcast?i=http://www.grailspodcast.com/blog/id/${entry.id}" type="text/javascript" charset="utf-8"></script>
    </div>
    <g:if test="${params.id}">
    	<div class="comments">
			<script>
			var idcomments_acct = '1665ee0d81610cb3b626fbe2eed48346';
			var idcomments_post_id = ${entry.id};
			var idcomments_post_url;
			</script>
			<span id="IDCommentsPostTitle" style="display:none"></span>
			<script type='text/javascript' src='http://www.intensedebate.com/js/genericCommentWrapperV2.js'></script>
	    </div>
    </g:if>
    <g:if test="${!params.id}">
	    <div class="comments">
			<g:link action="id" id="${entry.id}">Create and see comments</g:link>
	    </div>
    </g:if>
  </div>

</g:each>

<g:if test="${!params.id}">
  <div class="pagination">
    <g:paginate controller="blog" next="Next Page" prev="Previous Page" action="list" total="${Entry.count()}" />
  </div>
</g:if>